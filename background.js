"use strict";

const sendData = async (data) => {
  const response = await fetch("http://localhost:3000/tracking-data", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });

  return response.json();
};

const listenTabsEvents = () => {
  let prev = "";
  let current = "";
  chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.url) {
      current = changeInfo.url;
    }
    if (changeInfo.status === "complete" && prev !== current) {
      chrome.tabs.sendMessage(tabId, {
        message: "url_changed",
        prev,
        current,
      });
      prev = current;
    }
  });
};

const listenChangeIcon = () => {
  let previousDomain = "";
  chrome.runtime.onMessage.addListener(
    async (request, sender, sendResponse) => {
      if (request.message === "change_icon") {
        let response = await sendData(request.data);
        let icon = response.data ? response.data : "default";
        if (request.data.domain !== previousDomain) {
          previousDomain = request.data.domain;
          chrome.browserAction.setIcon({ path: `/assets/${icon}.png` });
        }
      }
    }
  );
};

listenTabsEvents();
listenChangeIcon();
