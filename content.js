chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.message === "url_changed") {
    let data = {
      prev: request.prev,
      current: request.current,
      href: document.referrer,
      domain: document.location.hostname,
    };
    chrome.runtime.sendMessage({
      message: "change_icon",
      data,
    });
  }
});
